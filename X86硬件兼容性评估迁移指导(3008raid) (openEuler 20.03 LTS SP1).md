X86硬件兼容性评估迁移指导(3008raid) (openEuler 20.03 LTS SP1)
# 介绍
3008 raid卡是华为生产的一款raid卡。
# 环境要求
## 硬件要求
 硬件要求如下表所示


项目 | 说明| 
----- | ----- | 
服务器 | 2288H V5 
CPU | Inter(R) Xeon(R) Gold 6266C CPU@3.00GHz 
RAID卡 | LTS SAS 3408 
## 操作系统要求
操作系统要求如下表所示


项目 | 说明 |
----- | ----- |
Centos Linux | 7.9.2009(Core)
Kernel | 3.10.0 x86_64
## 检查当前版本系统信息
cat /etc/os-release
![1.png](https://bbs-img.huaweicloud.com/data/forums/attachment/forum/202104/02/1313273im99ccexkucipyc.png)
# x2openEuler软件运行和配置信息采集
## 环境信息采集：
    1、采集工具拷贝到环境的"/home"目录下并解压

        tar zxvf x2openEuler_0.1.0_fdbb2b20.tar.gz

    2、进入工具，创建结果保存文件夹

        cd x2openEuler_0.1.0

        mkdir results

    3、执行下面命令开始采集数据

        python3 x2openEuler.pyc collect-conf-info -p ./results
## 查看采集结果：
板卡的采集结果保存在hardware_configure.json文件中

采集到的3008数据
![6.png](https://bbs-img.huaweicloud.com/data/forums/attachment/forum/202104/02/131528bwgvuyhn7vm4ew0x.png)
# 迁移结果人工分析
    
   从南向板卡兼容性清单网站(https://gitee.com/openeuler/website-v2/tree/feature-compatibility/data/compatibility)查找3008raid卡信息，如果没有找到，说明openeuler 20.03 LTS SP1 还不支持3008raid卡。请您在 https://gitee.com/openeuler/website-v2/tree/master/data/compatibility 网站上提交issue，openEuler团队sig组会及时进行处理。
# 卸载x2openEuler软件
分析完成后，执行下面命令删除x2openEuler软件包和采集结果

rm -rf  x2openEuler_0.1.0