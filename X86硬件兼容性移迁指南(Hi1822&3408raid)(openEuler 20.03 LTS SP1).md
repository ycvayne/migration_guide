X86硬件兼容性移迁指南(Hi1822&3408raid)(openEuler 20.03 LTS SP1)
----
# 介绍
Hi1822网卡是华为生产的一款网卡。
3408raid卡是华为生产的一款raid卡。
# 环境要求
## 硬件要求
硬件要求如下表所示

项目 | 说明 |  
----- | ----- |
服务器 | 2288H V5 
CPU | Inter(R) Xeon(R) Gold 6266C CPU @ 3.00GHz 
NIC卡 | Hi1822 
RIAD卡 | LTS SAS 3408 

## 操作系统要求
操作系统要求如下所示

项目 | 说明 |
----- | ----- | 
Centos Linux | 7.9.2009（Core)  
Kernel | 3.10.0  x86_64  

检查当前系统版本信息
cat /etc/os-release

![输入图片说明](https://images.gitee.com/uploads/images/2021/0402/154036_43bae790_8039520.png "屏幕截图.png")
# x2openEuler软件运行和配置信息采集

## 环境信息采集步骤

    1、采集工具拷贝到环境的"/home"目录下并解压
        tar zxvf x2openEuler_0.1.0_fdbb2b20.tar.gz
    2、进入工具，创建结果保存文件夹
        cd x2openEuler_0.1.0        
        mkdir results
    3、执行下面命令开始采集数据
        python3 x2openEuler.pyc collect-conf-info -p ./results

## 查看采集结果

  板卡的采集结果保存在hardware_configure.json文件中。    
  ### 采集的Hi1822网卡数据
![输入图片说明](https://images.gitee.com/uploads/images/2021/0402/154057_8ec52ac9_8039520.png "屏幕截图.png")
  ### 采集的3408 raid卡数据
![输入图片说明](https://images.gitee.com/uploads/images/2021/0402/154116_7da1aa7b_8039520.png "屏幕截图.png")
# 迁移结果人工分析
    
从南向板卡兼容性清单网站(https://gitee.com/openeuler/website-v2/tree/feature-compatibility/data/compatibility), 可以查找Hi1822和3408raid卡适配openEuler 20.03 LTS SP1系统，信息如下。

## Hi1822网卡适配信息
![输入图片说明](https://images.gitee.com/uploads/images/2021/0402/154134_c1c4317a_8039520.png "屏幕截图.png")

## 3408 raid卡适配信息
![输入图片说明](https://images.gitee.com/uploads/images/2021/0402/154153_c4f1817e_8039520.png "屏幕截图.png")

通过VID、DID、SVID、SSID四元组值可确定唯一一种板卡。从上面的截图可以看到，南向兼容性清单网站可以查询到Hi822卡和3408raid板卡，并且各板卡的四元组信息和采集到的板卡四元组信息相同，以此得出Hi1822板卡和3408raid从centos上迁移到openEuler 20.03 LTS SP1成功。

# 卸载x2openEuler软件
分析完成后，执行下面命令删除x2openEuler软件包和采集结果

rm -rf  x2openEuler_0.1.0